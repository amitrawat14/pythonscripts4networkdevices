### Tested script to find rule in Palo, Scripted is tested in Multi-Vsys environment.

from datetime import datetime
import paramiko,sys,re
import getpass


ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

user_name = 'amitsingrawat'
user_password ='EnterYourPassword'


source_addr = input(" Enter Source Address :")
dst_addr = input("Enter Destination Address :")
d_port = input("Enter destination port")

host_names= ['firewall_1_IP','Firewall_2_IP']  ## Add or remove IP for more firewalls if required.

def find_ip(addr):
    addr=addr.split('.')
    #print (addr)
    for ip in addr:
        ip= addr[0]+'.'+addr[1]+'.'+addr[2]
    #print(ip)
    return ip

##Calling find ip function to get the interface 
find_src = find_ip(source_addr)
find_dst = find_ip(dst_addr)
vsys_lists = []


def int_finder(session):
    command1 = (f'show routing route | match {find_src} \n').encode()
    session.send(command1)
    time.sleep(2)
    command1_recv = session.recv(65555).decode()
    src_int = re.findall(r'ae\S+\d\d\d\d?',command1_recv)
    #print (f'source inteface found {src_int}')
    command2 = (f'show routing route | match {find_dst} \n').encode()
    session.send(command2)
    time.sleep(2)
    command2_recv = session.recv(65555).decode()
    dst_int = re.findall(r'ae\S+\d\d\d\d?',command2_recv)
    print(f'print source interface {src_int} and destination interface {dst_int}')
    return src_int,dst_int



def Get_Src_Vsys(transport,host_name,src_int):
    session = transport.open_session()
    session.get_pty()
    session.invoke_shell()
    session.keep_this = ssh
    pager_cli ='set cli pager off \n'
    pager_cli = pager_cli.encode()
    session.send(pager_cli)
    time.sleep(1)
    pager_output = session.recv(1)
    time.sleep(1)
    src_vsys_list = []
    try:
        if src_int[0]==None:
            print('source address dont exist locally')
        else:
            for src in src_int:
                command3 = f'show interface {src} \n'
                command3 = bytes(command3,'utf-8')
                print(command3)
                session.send(command3)
                time.sleep(3)
                interface_data = session.recv(65555).decode()
                time.sleep(3)
                src_vsys=re.search(r'vsys\d\d?\d?',interface_data).group()
                print (f'\nSource Address found for interface {src} found in the FW {host_name} and part of Vsys {src_vsys}')
                src_vsys_list.append(src_vsys)
               
                
    except:
        print('source address not found')
    return src_vsys_list
    session.close()
        

########################

def Get_Dst_Vsys(transport,host_name,dst_int):
    session = transport.open_session()
    session.get_pty()
    session.invoke_shell()
    session.keep_this = ssh
    pager_cli ='set cli pager off \n'
    pager_cli = pager_cli.encode()
    session.send(pager_cli)
    time.sleep(1)
    pager_output = session.recv(1)
    time.sleep(1)
    dst_vsys_list = []
    try:
        print('searching destination interface to find dst vsys')
        print (dst_int)
        if dst_int[0]==None:
            print('destination address dont exist locally')
        else:
            for dst in dst_int:
                command4 = (f'show interface {dst} \n').encode()
                session.send(command4)
                print('checking if command is sent for destination vsys ')
                time.sleep(3)
                interface_data = session.recv(65999).decode()
                time.sleep(3)
                #print(interface_data)
                dst_vsys=re.search(r'vsys\d\d?\d?',interface_data).group()
                dst_vsys_list.append(dst_vsys)
                print (f'\nDestination Address for interface {dst} found in the FW {host_name}and part of Vsys {dst_vsys}')
        
    except:
        print('destination address not found')
    return dst_vsys_list
    session.close()


def vsys_int_call(host_name,src_int,dst_int):
    src_vsys = []
    ssh.connect (host_name, port = 22 , username =user_name , password = user_password)
    transport = ssh.get_transport()
    src_vsys = Get_Src_Vsys(transport,host_name,src_int)
    dst_vsys = Get_Dst_Vsys(transport,host_name,dst_int)
    vsys_lists = src_vsys + dst_vsys
    return vsys_lists

for host_name in host_names:
    ssh.connect (host_name, port = 22 , username =user_name , password = user_password)
    transport = ssh.get_transport()
    session = transport.open_session()
    session.get_pty()
    session.invoke_shell()
    session.keep_this = ssh
    print (f'\nLogin to Firewall...{host_name}....')
    print (f'\n ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
    print(f'Creating Metadata of Firewall to find rule......{host_name}')
    src_int,dst_int = int_finder(session)
    print('src and dst inter face list generate',src_int,dst_int)
    vsys_lists = vsys_int_call(host_name,src_int,dst_int)
    print('print final vsys list',vsys_lists)
    if len(vsys_lists) <=0:
        print(f"Source_Destination not found in the Firewall {host_name}. Checking next firewall if available.....")
    else:
        try:
            print (f'Metadata Generated in this firewall {host_name}...Searching Rule Now....')
            for vsys in set(vsys_lists):
                vsys_command =f'set system setting target-vsys {vsys} \n'
                print(f'Vsys found. Searching Rule in Vsys.....{vsys}')
                vsys_command_byte = vsys_command.encode()
                session.send(vsys_command_byte)
                time.sleep(2)
                test_command = f'test security-policy-match source {source_addr} destination {dst_addr} destination-port {d_port} protocol 6 \n'
                test_command_byte = test_command.encode()
                session.send(test_command_byte)
                time.sleep(2)
                x = session.recv(64999)
                time.sleep(2)
                print (f"*******************print rule details in the Vsys {vsys}************************************")
                print (x.decode())
                time.sleep(2)
                print (f"*******************End of Rule details************************************")
                print (f'\nSigning out from the firewall....{host_name}....bye...bye..')
                print (f'\n=================================================================================')
        except:
            print(f"Source_Destination not found in the Firewall {host_name}. Checking next firewall if available.....")
    session.close()    


